﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadingUI : MonoBehaviour {

    public static string LoadingLevel;
    public Text percentageText;
    // Use this for initialization
    void Start () {
        Time.timeScale = 1f;
        StartCoroutine(Load());
	}

    IEnumerator Load()
    {
        yield return new WaitForEndOfFrame();
        AsyncOperation asy = SceneManager.LoadSceneAsync(LoadingLevel);
        while(asy.isDone == false)
        {
            int perc = Mathf.RoundToInt(100 * asy.progress);
            percentageText.text = "Loading: " + perc + "%";
            yield return new WaitForEndOfFrame();
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
