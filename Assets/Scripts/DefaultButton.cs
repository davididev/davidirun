﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DefaultButton : MonoBehaviour {

    private void OnEnable()
    {
        if(Application.isMobilePlatform == false)
            EventSystem.current.SetSelectedGameObject(gameObject);
    }
}
