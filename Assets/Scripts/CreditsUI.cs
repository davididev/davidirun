﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CreditsUI : MonoBehaviour {
    public TextAsset creditsAsset;
    public Text creditsText;
	// Use this for initialization
	void Start () {
        StartCoroutine(Credits());
	}

    IEnumerator Credits()
    {
        float fadeTime = 0.5f;
        float timeToRead = 5f;

        string[] lines = creditsAsset.text.Split('\n');
        for(int i = 0; i < lines.Length; i++)
        {
            if (lines[i] != "\r")
            {
                creditsText.text = lines[i];
                Color c1 = Color.white;
                c1.a = 0f;
                Color c2 = Color.white;
                
                iTween.ValueTo(gameObject, iTween.Hash("from", c1, "to", c2, "time", fadeTime, "onupdate", "ChangeAlpha"));
                yield return new WaitForSeconds(fadeTime);
                yield return new WaitForSeconds(timeToRead);
                iTween.ValueTo(gameObject, iTween.Hash("from", c2, "to", c1, "time", fadeTime, "onupdate", "ChangeAlpha"));
                yield return new WaitForSeconds(fadeTime);
            }
        }

        SceneManager.LoadScene("Title");
    }

    void ChangeAlpha(Color c)
    {
        creditsText.color = c;
    }

    // Update is called once per frame
    void Update () {
		
	}
}
