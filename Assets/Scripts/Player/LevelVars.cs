﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelVars : MonoBehaviour {
    public static float gravityScale = 1.0f;
    public float _gravityScale = 1.0f;
    public static float portalX;
    public float _portalX = 3f;
    public static bool moveTwoDirs = false;
    public bool _moveTwoDirs = false;
    public bool _levelClearsOnEnemyKilled = false;
    public static bool levelClearsOnEnemyKilled;
    public float _runningSpeedMultiplier = 1f;
    public static float runningSpeedMultiplier;
    public string nextLevelName;
    public int timeLimit = -1;
    public bool sideScroller = true;  //If false, this will go F-Zero style.  Need some thoughts on the code.

    public static string timeTxt = "";

    public AudioClip levelMusic;
    public static AudioSource levelMusicHolder;
    private float secondsLeft;

	// Use this for initialization
	void Start () {
        
        gravityScale = _gravityScale;
        moveTwoDirs = _moveTwoDirs;
        levelClearsOnEnemyKilled = _levelClearsOnEnemyKilled;
        portalX = _portalX;
        AudioSource mine = gameObject.AddComponent<AudioSource>();
        mine.loop = true;
        mine.clip = levelMusic;
        mine.Play();
        levelMusicHolder = mine;
        timeTxt = "";
        secondsLeft = timeLimit;
        runningSpeedMultiplier = _runningSpeedMultiplier;
    }

    private void OnDestroy()
    {
        levelMusicHolder = null;
    }
    // Update is called once per frame
    void Update () {
		if(timeLimit > 0 && PlayerUI.levelCompleted == false)
        {
            if (PlayerPrefs.GetInt("Time") == 0)
            {
                secondsLeft -= Time.deltaTime;
                if (secondsLeft < 0f)
                    secondsLeft = 0f;
                int m = 0, s = (int)secondsLeft;

                while (s > 60)
                {
                    m++;
                    s -= 60;

                }
                if (s >= 10)
                    timeTxt = m + ":" + s;
                else
                    timeTxt = m + ":0" + s;

                if (secondsLeft <= 0f)
                {
                    FindObjectOfType<PlayerController>().Damage(100);  //Kill the player
                }
            }
        }
	}
}
