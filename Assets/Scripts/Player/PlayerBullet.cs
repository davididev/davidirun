﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBullet : MonoBehaviour {

    public float direction = 1.0f;
    public float vDir = 0.0f;
    private Rigidbody2D rigid;
    private float lifeTimer = 0f;
    private const float LIFE_TIME = 2.5f;
	// Use this for initialization
	void Start () {
        rigid = GetComponent<Rigidbody2D>();
	}

    private void OnEnable()
    {
        lifeTimer = 0f;
    }

    // Update is called once per frame
    void Update () {
        rigid.velocity = (Vector2.right * direction * 16.0f) + (Vector2.up * vDir);
        lifeTimer += Time.deltaTime;
        if (lifeTimer > LIFE_TIME)
            gameObject.SetActive(false);

        if (PlayerController.cam != null)
        {
            Vector3 screenPos = PlayerController.cam.WorldToScreenPoint(transform.position);
            if (screenPos.x > (Screen.width * 1.1f))
            {
                gameObject.SetActive(false);
            }
            if (screenPos.x < (Screen.width * -0.1f))
            {
                gameObject.SetActive(false);
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Handle enemy collisions soon.
        Vector2 norm = collision.contacts[0].normal;
        if(collision.collider.gameObject.layer == 12 && Mathf.Abs(norm.y) < 0.1f)  //Tiles
        {
            gameObject.SetActive(false);
        }

        Enemy e = collision.gameObject.GetComponent<Enemy>();
        if(e != null)
        {
            e.Damage();
            gameObject.SetActive(false);
        }
    }
}
