﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerController : MonoBehaviour {

    private float CAMERA_Z;
    public int _maxHealth = 2;
    private int _health;
    public static int maxHealth;
    public static int health;
    private const float MAX_VELOCITY = 7.5f;
    private const float MOVE_FORCE = 20.0f;
    private const float VELOCITY_MULTIPLIER = 2.0f;  //this value should be 1.0 timescale
    private Rigidbody2D rigid;
    private Animator anim;
    private float facing = 1.0f;
    private bool jumping = false;
    private bool firing = false;
    private LayerMask collisionMask;
    private Camera mainCamera;
    public static Camera cam;
    private SpriteRenderer rend;
    private bool damaging = false;

    [HideInInspector] public PlayerRoot root;

    public bool pauseMovement { private set; get; } 
    public void PauseMovement()
    {
        pauseMovement = true;
        Vector2 v = rigid.velocity;
        v.x = 0f;
        rigid.velocity = v;
    }

    public void Damage(int amt, bool isLava = false)
    {
        if (PlayerPrefs.GetInt("Immortal") == 0 || isLava == true)
        {
            if (damaging == false)
            {
                _health -= amt;
                health = _health;
                StartCoroutine(DamageBlink());
            }
        }
    }

    /// <summary>
    /// Stop ALL movement, ALL controls, etc
    /// </summary>
    private bool frozen = false;
    public void Freeze()
    {
        frozen = true;
    }

	IEnumerator IGotStuck()
	{
		while(gameObject == true)
		{
			Vector2 lastPos = rigid.position;
			yield return new WaitForSeconds(1.0f);
		
			if(frozen == false)
			{
				Vector2 dif = lastPos - rigid.position;
				if(Vector2.Distance(lastPos, dif) < 0.5f)
				{
					facing = facing * -1f;
				}
			}
		}
	}
	
    IEnumerator DamageBlink()
    {
        if (_health <= 0)
        {
            root.OnDie();
            //gameObject.SetActive(false);
        }
        else
        {
            damaging = true;
            for (int i = 0; i < 30; i++)
            {
                if (i % 2 == 1)
                    rend.color = Color.white;
                else
                    rend.color = Color.clear;
                yield return new WaitForSeconds(0.1f);
            }
        }
        rend.color = Color.white;
        damaging = false;
    }

    // Use this for initialization
    void Start () {
        pauseMovement = false;
        rend = GetComponent<SpriteRenderer>();
        _health = _maxHealth;
        maxHealth = _maxHealth;
        health = _health;
        mainCamera = GameObject.FindGameObjectWithTag("Camera").GetComponent<Camera>();
        CAMERA_Z = mainCamera.transform.position.z;
        cam = mainCamera;
        rigid = GetComponent<Rigidbody2D>();
        collisionMask = LayerMask.GetMask("Tiles");
        if (rigid == null)
        {
            Debug.LogError("Rigid body missing.  PlayerController " + gameObject.name + " will not work.");
        }
        else
        {
            rigid.drag = 2.0f;
            rigid.constraints = RigidbodyConstraints2D.FreezeRotation;
            UpdateGravityScale();
        }

        anim = GetComponent<Animator>();
        if(anim == null)
        {
            Debug.LogError("Animator missing.  PlayerController " + gameObject.name + " will not work.");
        }
		
		if(LevelVars.moveTwoDirs == true)
		{
			StartCoroutine(IGotStuck());
		}
	}
    
    public void UpdateGravityScale()
    {
        rigid.gravityScale = LevelVars.gravityScale * transform.localScale.y;
    }


    // Update is called once per frame
    void FixedUpdate () {
        if(frozen == true)
        {
            //Let's stop everything.
            rigid.velocity = Vector2.zero;
            anim.SetFloat("Timescale", 0f);
            return;
        }
        //CAMERA_Z is not literally a constant but should be treated as one.
        //It is relevant distance to the player, and if we ever do an F-Zero style level,
        //these are the lines that we need to change to update the camera.
        mainCamera.transform.position = new Vector3(rigid.position.x, rigid.position.y + -3.0f, transform.position.z + CAMERA_Z);
        //UpdateOrthoSize();

        anim.SetFloat("Facing", facing);
        anim.SetFloat("Timescale", (Mathf.Abs(rigid.velocity.x) / VELOCITY_MULTIPLIER));
        if (pauseMovement == false)
        {
            if(Mathf.Abs(rigid.velocity.x) < MAX_VELOCITY * LevelVars.runningSpeedMultiplier)
                rigid.AddForce(facing * Vector2.right * MOVE_FORCE * LevelVars.runningSpeedMultiplier * Time.timeScale, ForceMode2D.Force);
        }
        if(JumpButton.isJumping == true && jumping == false)  //Start jump
        {
            if(pauseMovement)
            {
                pauseMovement = false;
                jumping = true;
            }
            else
            {
				Vector2 upVec = (transform.localScale.y > 0) ? Vector2.up : Vector2.down;
                if (Physics2D.Raycast(transform.position, -upVec, 1.15f, collisionMask.value))
                {
                    AudioSource.PlayClipAtPoint(root.jumpSound, transform.position);
                    Debug.Log("Boing!");
                    rigid.AddForce(upVec * 1000.0f * Time.timeScale, ForceMode2D.Force);
                    jumping = true;
                }
            }
            
        }
        if (JumpButton.isJumping == false && jumping == true)  //End jump
        {
            Vector2 vel = rigid.velocity;
            if(vel.y > 0.0f)
            {
                vel.y = 0.0f;
                rigid.velocity = vel;
                
            }
            jumping = false;
        }

        if(firing == false && AttackButton.isAttacking == true)
        {
            if (PlayerRoot.stars > 0)
            {
                firing = true;
                root.SpawnBullet(transform.position, facing, 0.0f);
                if(PlayerRoot.stars >= 10)  //Make a tri shoot
                {
                    root.SpawnBullet(transform.position, facing, 1.0f);
                    root.SpawnBullet(transform.position, facing, -1.0f);
                }
                if(PlayerPrefs.GetInt("Ammo") == 0)    //Only decrement if infinite ammo is turned off
                    PlayerRoot.stars--;
            }
        }
        if (AttackButton.isAttacking == false)
            firing = false;

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Vector2 norm = collision.contacts[0].normal;
        if (norm.y == 0.0f)  //Not a slope or floor or ceiling- must be a wall
        {
            if(LevelVars.moveTwoDirs)
            {
                //Swap facing- it's one of those levels
                if (norm.x < 0.0f)
                    facing = -1f;
                if (norm.x > 0.0f)
                    facing = 1f;
            }
            else
            {
                //Bounce of walls- regular run level

                if (norm.x < 0.0f)
                    rigid.AddForce(-5f * Vector2.right * MOVE_FORCE * Time.timeScale, ForceMode2D.Force);
                if (norm.x > 0.0f)
                    rigid.AddForce(5f * Vector2.right * MOVE_FORCE * Time.timeScale, ForceMode2D.Force);
            }
        }
    }
}
