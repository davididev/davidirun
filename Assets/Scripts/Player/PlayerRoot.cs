﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerRoot : MonoBehaviour {

    public PlayerController[] controllers;
    public PlayerUI playerUI;
    private SpriteRenderer dyingRend;
    public static int selectedPlayer = PLAYER_NAOMI;
    public const int PLAYER_BECKY = 0;
    public const int PLAYER_NATE = 1;
    public const int PLAYER_TIM = 2;
    public const int PLAYER_NAOMI = 3;

    public AudioClip jumpSound, dieSound;
    public GameObject bulletPrefab;
    private GameObject[] bulletPool;
    public AudioClip shootSound;
    public static int stars = 0;
    // Use this for initialization
    void Start () {
        controllers[selectedPlayer].gameObject.SetActive(true);
        controllers[selectedPlayer].root = this;
        stars = 0;
        bulletPool = new GameObject[50];
        for(int i = 0; i < bulletPool.Length; i++)
        {
            bulletPool[i] = Instantiate(bulletPrefab, transform);
            bulletPool[i].SetActive(false);
        }
    }

    public void SpawnBullet(Vector3 pos, float facing, float vDir)
    {
        for(int i = 0; i < bulletPool.Length; i++)
        {
            if(bulletPool[i].activeSelf == false)
            {
                bulletPool[i].SetActive(true);
                bulletPool[i].transform.position = pos;
                PlayerBullet pb = bulletPool[i].GetComponent<PlayerBullet>();
                pb.direction = facing;
                pb.vDir = vDir;
                AudioSource.PlayClipAtPoint(shootSound, transform.position);
                break;
            }
        }
    }
    public void OnDie()
    {
        StartCoroutine(DieRoutuine());
    }


    /// <summary>
    /// Internal function to go with DieRoutine.  iTween apparently doesn't account for sprite renderers so we have to make our own.
    /// dyingRend is obtained at DieRoutine.
    /// </summary>
    /// <param name="alpha"></param>
    private void DieFade(float alpha)
    {
        Color c = dyingRend.color;
        c.a = alpha;
        dyingRend.color = c;
    }

    /// <summary>
    /// The coroutine that plays every time the player dies
    /// </summary>
    /// <returns></returns>
    IEnumerator DieRoutuine()
    {
        //Technically not constants but I'm going to name them as such.  They are relevant to only THIS coroutine.
        float DIE_TIME = 2.0f;
        float DELAY_AFTER_DEATH = 1.5f;

        GameObject g = controllers[selectedPlayer].gameObject;
        g.layer = 2;  //Ignore raycast so you cannot collide with level geo.
        controllers[selectedPlayer].Freeze();
        AudioSource.PlayClipAtPoint(dieSound, g.transform.position, 1f);
        dyingRend = g.GetComponent<SpriteRenderer>();


        Vector3 sc = new Vector3();  //Heck, let's randomize the "explosion" with scale.
        int rand = Random.Range(1, 3);
        if(rand == 1)
            sc.Set(0.01f, 2f, 1f);
        if (rand == 2)
            sc.Set(2f, 0.01f, 1f);
        if (rand == 3)
            sc.Set(2f, 2f, 1f);
        iTween.ScaleTo(g, sc, DIE_TIME);

        //The fade needs to be 80% or so of the actual time so it doesn't look weird.
        iTween.ValueTo(gameObject, iTween.Hash("from", 1f, "to", 0f, "onupdate", "DieFade", "time", DIE_TIME * 0.8f));  

        yield return new WaitForSeconds(DIE_TIME);
        g.SetActive(false);
        yield return new WaitForSeconds(DELAY_AFTER_DEATH);
        string scene = SceneManager.GetActiveScene().name;
        LoadingUI.LoadingLevel = scene;
        SceneManager.LoadScene("Loading");
    }

    // Update is called once per frame
    void Update () {
        if (LevelVars.levelClearsOnEnemyKilled == true && Enemy.count == 0)
            playerUI.EndLevel();

    }
}
