﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerUI : MonoBehaviour {

    public Text starText, goalText, timerTxt;
    public Image[] healths;
    public Sprite healthEmpty, healthFull;
    public GameObject onLevelComplete, pausePanel;
    public AudioClip fanfare;
    public static bool levelCompleted = false;
    // Use this for initialization
    private void Awake()
    {
        levelCompleted = false;
    }
    void Start () {
        StartCoroutine(DisplayGoalText());

        timerTxt.text = "";

    }

    public void ReturnToTitle()
    {
        SceneManager.LoadScene("Title");
    }

    public void TogglePause()
    {
        pausePanel.SetActive(!pausePanel.activeSelf);
        if (pausePanel.activeSelf == true)
            Time.timeScale = 0f;
        if (pausePanel.activeSelf == false)
            Time.timeScale = 1f;
    }

    bool startedLevelEnd = false;
    public void EndLevel()
    {
        levelCompleted = true;
        if (startedLevelEnd == false)
        {
            startedLevelEnd = true;
            StartCoroutine(CompleteLevel());
        }
    }

    IEnumerator DisplayGoalText()
    {
        yield return new WaitForEndOfFrame();
        if(LevelVars.levelClearsOnEnemyKilled == true)
        {
            goalText.text = "Clear all enemies.";
        }
        else
        {
            goalText.text = "Find the goal.";
        }
        if (LevelVars.timeTxt != "")
            goalText.text = goalText.text + "\nWatch the timer.";
        goalText.gameObject.SetActive(true);
        yield return new WaitForSeconds(5f);
        goalText.gameObject.SetActive(false);
    }

    IEnumerator CompleteLevel()
    {
        LevelVars.levelMusicHolder.clip = fanfare;
        LevelVars.levelMusicHolder.loop = false;
        LevelVars.levelMusicHolder.Play();
        onLevelComplete.SetActive(true);
        yield return new WaitForSeconds(4f);
        LevelVars lv = FindObjectOfType<LevelVars>();
        LoadingUI.LoadingLevel = lv.nextLevelName;
        SceneManager.LoadScene("Loading");
    }
	
	// Update is called once per frame
	void Update () {
        timerTxt.text = LevelVars.timeTxt;
        starText.text = "x" + PlayerRoot.stars;
        for (int i = 0; i < healths.Length; i++)
        {
            healths[i].gameObject.SetActive(i < PlayerController.maxHealth);
            if (PlayerController.health > i)
                healths[i].sprite = healthFull;
            else
                healths[i].sprite = healthEmpty;
        }

        if (Input.GetKeyDown(KeyCode.Escape))
            TogglePause();
        if (Input.GetKeyDown(KeyCode.Return))
            TogglePause();
    }
}
