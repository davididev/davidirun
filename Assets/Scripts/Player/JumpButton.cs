﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class JumpButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{

    public static bool isJumping = false;

	// Use this for initialization
	void Start () {
        Physics.queriesHitTriggers = true;

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            isJumping = true;
            Debug.Log("Jump pressed.");
        }
        if (Input.GetKeyUp(KeyCode.Alpha1))
        {
            isJumping = false;
            Debug.Log("Jump released.");
        }

        if (Input.GetKeyDown(KeyCode.Keypad1))
        {
            isJumping = true;
            Debug.Log("Jump pressed.");
        }
        if (Input.GetKeyUp(KeyCode.Keypad1))
        {
            isJumping = false;
            Debug.Log("Jump released.");
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        isJumping = true;
        Debug.Log("Jump pressed.");
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        isJumping = false;
        Debug.Log("Jump released.");
    }


}
