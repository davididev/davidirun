﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class AttackButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{

    public static bool isAttacking = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            isAttacking = true;
            Debug.Log("Attack pressed.");
        }
        if (Input.GetKeyUp(KeyCode.Alpha2))
        {
            isAttacking = false;
            Debug.Log("Attack released.");
        }
        if (Input.GetKeyDown(KeyCode.Keypad2))
        {
            isAttacking = true;
            Debug.Log("Attack pressed.");
        }
        if (Input.GetKeyUp(KeyCode.Keypad2))
        {
            isAttacking = false;
            Debug.Log("Attack released.");
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        isAttacking = true;
        Debug.Log("Attack pressed.");
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        isAttacking = false;
        Debug.Log("Attack released.");
    }
}
