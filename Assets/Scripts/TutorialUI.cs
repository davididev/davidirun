﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TutorialUI : MonoBehaviour {

    public GameObject uiControlsMobile, uiControlsWeb;
    public Text headerText, uiControlsText1, uiControlsText2;

	// Use this for initialization
	void Start () {
        StartCoroutine(Tutorial());
	}

    IEnumerator Tutorial()
    {
        GameObject controlsPointer = uiControlsWeb;
        if (Application.isMobilePlatform)
            controlsPointer = uiControlsMobile;

        controlsPointer.SetActive(true);
        uiControlsText1.text = "Jump";
        uiControlsText2.text = "Attack";
        headerText.text = "Press buttons as shown for actions.";
        if(Application.isMobilePlatform == false)
            headerText.text = headerText.text + "\n(1: Jump.  2: Attack)";
        yield return new WaitForSeconds(5f);
        uiControlsText1.text = "";
        uiControlsText2.text = "";
        headerText.text = "";
        controlsPointer.SetActive(false);

        while(PlayerRoot.stars == 0)
        {
            yield return new WaitForEndOfFrame();
        }

        headerText.text = "Stars are ammo.";
        yield return new WaitForSeconds(5f);
        headerText.text = "";

        PlayerController pc = FindObjectOfType<PlayerController>();
        if(pc != null)
        {
            while(pc.pauseMovement == false)
            {
                yield return new WaitForEndOfFrame();
            }

            controlsPointer.SetActive(true);
            uiControlsText1.text = "Resume run";
            headerText.text = "Stop signs are the only way to stop running.";
            yield return new WaitForSeconds(5f);
            uiControlsText1.text = "";
            headerText.text = "";
            controlsPointer.SetActive(false);
            
        }
        else
        {
            Debug.LogError("Cannot find player controller.  This isn't supposed to happen!  Cannot show whole tutorial.  >:");
        }

        
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
