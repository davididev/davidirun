﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : ObstacleBase {

    public float initialDelay = 1f;
    public float height = 5f;
    public float peakTime = 2f;

    private Vector3 startingPos;
    private SpriteRenderer rend;
    private Collider2D col;
    private const float USUAL_DELAY = 1f;

    // Use this for initialization
    void Start () {
        col = GetComponent<Collider2D>();
        rend = GetComponent<SpriteRenderer>();
        
    }

    void SetHidden(bool b)
    {
        col.enabled = !b;
        rend.color = (b == true) ? Color.clear : Color.white;
    }

    protected override IEnumerator ObstacleRoutine()
    {
        startingPos = transform.position;
        SetHidden(true);
        yield return new WaitForSeconds(initialDelay);
        while (gameObject)
        {
            SetHidden(false);
            iTween.MoveTo(gameObject, iTween.Hash("position", (startingPos + (Vector3.up * height)), "time", peakTime, "easetype", iTween.EaseType.easeOutQuad));
            yield return new WaitForSeconds(peakTime);
            iTween.MoveTo(gameObject, iTween.Hash("position", startingPos, "time", peakTime, "easetype", iTween.EaseType.easeInQuad));
            yield return new WaitForSeconds(peakTime);
            transform.position = startingPos;
            SetHidden(true);
            yield return new WaitForSeconds(USUAL_DELAY);
        }

    }

    // Update is called once per frame
    void Update () {
		
	}
}
