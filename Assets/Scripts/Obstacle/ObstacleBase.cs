﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleBase : MonoBehaviour {

    private bool started = false;
	// Use this for initialization
	void Start () {
		
	}

    private void OnBecameVisible()
    {
        if(started == false)
        {
            StartCoroutine(ObstacleRoutine());
            started = true;
        }
        
    }

    protected virtual IEnumerator ObstacleRoutine()
    {
        yield return null;
    }

    // Update is called once per frame
    void Update () {
		
	}
}
