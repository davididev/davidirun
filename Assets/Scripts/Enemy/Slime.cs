﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slime : Enemy {

    public float facing = -1f;
    public float moveSpeed = 60.0f;
    public float maxSpeed = 10f;
    // Use this for initialization


    protected override IEnumerator EnemyScript()
    {
        while(gameObject)
        {
            if(Mathf.Abs(rigid.velocity.x) < maxSpeed)
                rigid.AddForce(Vector2.right * facing * moveSpeed * Time.timeScale);
            if (Physics2D.Raycast(rigid.position, Vector2.right * facing, 1.0f, tileMask))
                facing = facing * -1f;
            yield return new WaitForEndOfFrame();
        }
    }

    
}
