﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    bool runningScript = false;
    public int touchDamage = 1;
    public int health = 1;
    protected Rigidbody2D rigid;
    protected int tileMask;
    public static int count = 0;
    public float myGravityScale = 1;
	
	protected Vector2 upVec;
    // Use this for initialization
    protected void Start () {
        rigid = GetComponent<Rigidbody2D>();
        UpdateGravityScale();
        tileMask = LayerMask.GetMask("Tiles");
    }

    public void UpdateGravityScale()  //Should be able to be called for the gravity flipper.
    {
        rigid.gravityScale = LevelVars.gravityScale * myGravityScale * transform.localScale.y;
    }

    public virtual void OnUpdate()
    {

    }

    private void OnEnable()
    {
        count++;
    }

    private void OnDisable()
    {
        count--;
        runningScript = false;
        StopAllCoroutines();
    }

    public void Damage(int amt = 1)  //Will probably never make anything stronger, but just in case...
    {
        health -= 1;
        if(health <= 0)
            gameObject.SetActive(false);
    }

    protected virtual IEnumerator EnemyScript()
    {
        yield return null;
    }
    
	// Update is called once per frame
	void Update () {
		upVec = (transform.localScale.y > 0) ? Vector2.up : Vector2.down;
        OnUpdate();  //In case we need an override script
	}

    private void OnBecameVisible()
    {
        if(runningScript == false)
        {
            StartCoroutine(EnemyScript());
        }
    }

    protected void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            PlayerController pc = collision.gameObject.GetComponent<PlayerController>();
            if (pc != null)
                pc.Damage(touchDamage);
        }
    }
}
