﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bat : Enemy {

    public float checkRadius = 5f;
    public float moveSpeed = 2f;
    public float startingRadius = 1f;
    private Vector3 startingPos;
    bool foundPlayer = false;
    int lm = -1;
    Rigidbody2D lockon;

    public override void OnUpdate()
    {
        if (lm == -1)
            lm = LayerMask.GetMask("Player");
        if(foundPlayer == false)
        {
            Collider2D player = Physics2D.OverlapCircle(rigid.position, checkRadius, lm);
            if(player != null)
            {
                foundPlayer = true;
                lockon = player.attachedRigidbody;
            }
        }
    }

    protected override IEnumerator EnemyScript()
    {
        while (health > 0)
        {
            startingPos = transform.position;
            foundPlayer = false;
            while (!foundPlayer)
            {
                Vector3 targetPos = startingPos + (Vector3.right * Random.Range(-startingRadius, startingRadius)) + (Vector3.up * Random.Range(-startingRadius, startingRadius));
                while (Vector3.Distance(transform.position, targetPos) > 0.1f)
                {
                    rigid.velocity = Vector2.zero;
                    transform.position = Vector3.MoveTowards(transform.position, targetPos, moveSpeed * Time.deltaTime);
                    yield return new WaitForEndOfFrame();
                }


            }

            Vector2 rel = (lockon.position - rigid.position).normalized;
            rigid.AddForce(rel * moveSpeed * 750f * Time.timeScale);
            yield return new WaitForFixedUpdate();
        }
    }
}
