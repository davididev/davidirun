﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BouncingEnemy : Enemy {

    //Requires a "jump" trigger

    public float jumpForce = 500f;
    public float delaysBetweenHops = 0.25f;
    private bool isGrounded = false;
    private float scanDist = 0f;
    private int lm;

    public override void OnUpdate()
    {
        if(scanDist != 0f)
        {
            isGrounded = Physics2D.Raycast(transform.position, -upVec, scanDist, lm);
        }
    }

    protected override IEnumerator EnemyScript()
    {
        Vector3 extents = GetComponent<Collider2D>().bounds.size;
        scanDist = extents.y * 0.55f;
        Animator anim = GetComponent<Animator>();
        lm = LayerMask.GetMask("Tiles");
        while(health > 0)
        {
            while(isGrounded == false) { yield return new WaitForFixedUpdate();  }
            yield return new WaitForSeconds(delaysBetweenHops);
            rigid.AddForce(upVec * jumpForce * Time.timeScale);
            anim.SetTrigger("Jump");
            yield return new WaitForSeconds(0.1f);
            
        }
    }
}
