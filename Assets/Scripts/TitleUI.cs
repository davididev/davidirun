﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TitleUI : MonoBehaviour {

    public GameObject mainPanel, levelPanels, cheatsPanel;
    [System.Serializable]
    public struct CheatButton
    {
        public string cheatName;
        public string cheatKey;
        public Text CheatsText;
    }
    public CheatButton[] cheatButtons;

	// Use this for initialization
	void Start () {
		
	}

    public void GoToLevels()
    {
        mainPanel.SetActive(false);
        levelPanels.SetActive(true);
    }

    public void GoToCheats()
    {
        mainPanel.SetActive(false);
        cheatsPanel.SetActive(true);
        UpdateCheatTexts();
    }

    private void UpdateCheatTexts()
    {
        for(int i = 0; i < cheatButtons.Length; i++)
        {
            string str1 = cheatButtons[i].cheatName;
            string str2 = (PlayerPrefs.GetInt(cheatButtons[i].cheatKey) == 1) ? "On" : "Off";
            cheatButtons[i].CheatsText.text = str1 + "\n" + str2;
        }
    }

    public void GoToTutorial()
    {
        LoadingUI.LoadingLevel = "Tutorial";
        SceneManager.LoadScene("Loading");
    }

    public void ToggleCheat(int id)
    {
        int status = PlayerPrefs.GetInt(cheatButtons[id].cheatKey);  //Toggle status
        //Toggle it
        if (status == 1)
            status = 0;
        else
            status = 1;
        cheatButtons[id].CheatsText.text = cheatButtons[id].cheatName + "\n" + (status == 1 ? "On" : "Off");
        PlayerPrefs.SetInt(cheatButtons[id].cheatKey, status);
        UpdateCheatTexts();
    }

    public void GoToCredits()
    {
        //Credits will probably have their own scene eventually
        LoadingUI.LoadingLevel = "Credits";
        SceneManager.LoadScene("Loading");
    }

    public void BeckyW1()
    {
        GoToLevel("Becky", 1);
    }

    public void NateW1()
    {
        GoToLevel("Nate", 1);
    }

    public void TimW1()
    {
        GoToLevel("Tim", 1);
    }

    public void NaaW1()
    {
        GoToLevel("Naa", 1);
    }

    public void QuitButton()
    {
        Application.Quit();
    }

    public void GoToLevel(string charName, int worldNum)
    {
        if(charName == "Becky")
            PlayerRoot.selectedPlayer = PlayerRoot.PLAYER_BECKY;
        if (charName == "Nate")
            PlayerRoot.selectedPlayer = PlayerRoot.PLAYER_NATE;
		if (charName == "Tim")
            PlayerRoot.selectedPlayer = PlayerRoot.PLAYER_TIM;
        if (charName == "Naa")
            PlayerRoot.selectedPlayer = PlayerRoot.PLAYER_NAOMI;
        LoadingUI.LoadingLevel = charName + "W" + worldNum + "L1";
        SceneManager.LoadScene("Loading");
    }

    public void GoToMain()
    {
        mainPanel.SetActive(true);
        levelPanels.SetActive(false);
        cheatsPanel.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
