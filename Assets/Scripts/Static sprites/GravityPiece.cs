﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityPiece : MonoBehaviour {

    public AudioClip flipSound;
	private bool isDiabled = false;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(Vector3.forward * 90f * Time.deltaTime);
	}

	IEnumerator DisableMe()
	{
		isDiabled = true;
		yield return new WaitForSeconds(0.1f);
		isDiabled = false;
	}
	
    private void OnTriggerEnter2D(Collider2D collision)
    {
		if(isDiabled)
			return;
        if(collision.tag == "Player" || collision.tag == "Enemy")
        {
			
			StartCoroutine(DisableMe());
            Vector3 scale = collision.gameObject.transform.localScale;
            scale.y = scale.y * -1f;
            collision.gameObject.transform.localScale = scale;
            AudioSource.PlayClipAtPoint(flipSound, transform.position);
            collision.gameObject.SendMessage("UpdateGravityScale", SendMessageOptions.DontRequireReceiver);
        }
    }
}
