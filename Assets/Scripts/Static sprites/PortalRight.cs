﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalRight : MonoBehaviour {

    public AudioClip warpSound;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player" || collision.tag == "Enemy")
        {
            Vector3 pos = collision.transform.position;
            pos.x = LevelVars.portalX;
            collision.transform.position = pos;
            if(warpSound != null)
                AudioSource.PlayClipAtPoint(warpSound, pos);
        }
    }
}
