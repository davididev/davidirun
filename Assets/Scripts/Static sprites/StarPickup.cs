﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarPickup : MonoBehaviour {

    public AudioClip pickupSound;
    private bool hidden = false;
    private float respawnTimer = 0f;
    private const float RESPAWN_TIME = 60f;
    private SpriteRenderer rend;

    public void SetHidden(bool h)
    {
        hidden = h;
        rend.color = (h == true) ? Color.clear : Color.white;
        if(h == true)
        {
            respawnTimer = RESPAWN_TIME;
        }
    }

	// Use this for initialization
	void Start () {
        rend = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
		if(respawnTimer > 0f)
        {
            respawnTimer -= Time.deltaTime;
            if (respawnTimer <= 0f)
                SetHidden(false);
        }
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (hidden == false)
        {
            AudioSource.PlayClipAtPoint(pickupSound, transform.position);
            PlayerRoot.stars++;
            SetHidden(true);
        }
        
    }
}
